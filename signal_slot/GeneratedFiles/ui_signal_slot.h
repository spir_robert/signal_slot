/********************************************************************************
** Form generated from reading UI file 'signal_slot.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIGNAL_SLOT_H
#define UI_SIGNAL_SLOT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_signal_slotClass
{
public:
    QPushButton *pushButton;
    QLabel *label;
    QPushButton *pushButton_2;
    QLabel *label_2;

    void setupUi(QDialog *signal_slotClass)
    {
        if (signal_slotClass->objectName().isEmpty())
            signal_slotClass->setObjectName(QStringLiteral("signal_slotClass"));
        signal_slotClass->resize(600, 400);
        pushButton = new QPushButton(signal_slotClass);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(50, 40, 75, 23));
        label = new QLabel(signal_slotClass);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(60, 90, 47, 13));
        pushButton_2 = new QPushButton(signal_slotClass);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(50, 200, 151, 31));
        label_2 = new QLabel(signal_slotClass);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(60, 250, 47, 13));

        retranslateUi(signal_slotClass);
        QObject::connect(pushButton, SIGNAL(clicked()), signal_slotClass, SLOT(gombik_klic()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), signal_slotClass, SLOT(value_klik()));

        QMetaObject::connectSlotsByName(signal_slotClass);
    } // setupUi

    void retranslateUi(QDialog *signal_slotClass)
    {
        signal_slotClass->setWindowTitle(QApplication::translate("signal_slotClass", "signal_slot", 0));
        pushButton->setText(QApplication::translate("signal_slotClass", "PushButton", 0));
        label->setText(QApplication::translate("signal_slotClass", "TextLabel", 0));
        pushButton_2->setText(QApplication::translate("signal_slotClass", "daj value klasy", 0));
        label_2->setText(QApplication::translate("signal_slotClass", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class signal_slotClass: public Ui_signal_slotClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIGNAL_SLOT_H
