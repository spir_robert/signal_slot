﻿#pragma once
#include <QDialog>
#include "ui_dialog.h"

class dialog : public QDialog {
	Q_OBJECT

public:
	dialog(QDialog * parent = Q_NULLPTR);
	~dialog();
signals:
	void trackk(int value);
public slots:
	void trackbar_vc(int value);

private:
	Ui::dialog ui;
};
