#include "signal_slot.h"

signal_slot::signal_slot(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	connect(&dg, &dialog::trackk, this, &signal_slot::track);
	connect(&dg, &dialog::trackk, &klas, &QtKlas::change_value);
}

signal_slot::~signal_slot()
{

}

void signal_slot::track(int value)
{
	ui.label->setText(QString::number(value));
}

void signal_slot::value_klik()
{
	this->ui.label_2->setText(QString::number(klas.get_value()));
}

void signal_slot::gombik_klic()
{
	dg.show();
}
