#ifndef SIGNAL_SLOT_H
#define SIGNAL_SLOT_H

#include <QtWidgets/QDialog>
#include "ui_signal_slot.h"
#include "dialog.h"
#include <QMessageBox>
#include "qtklas.h"

class signal_slot : public QDialog
{
	Q_OBJECT

public:
	signal_slot(QWidget *parent = 0);
	~signal_slot();
public slots:
	void gombik_klic();
	void track(int value);
	void value_klik();

private:
	Ui::signal_slotClass ui;
	dialog dg;
	QtKlas klas;
};

#endif // SIGNAL_SLOT_H
