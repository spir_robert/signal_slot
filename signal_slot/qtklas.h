﻿#pragma once
#include <QObject>

class QtKlas : public QObject {
	Q_OBJECT

public:
	QtKlas(QObject * parent = Q_NULLPTR);
	~QtKlas();
	int get_value();
public slots:
	void change_value(int val);
private:
	int value = 0;
};
